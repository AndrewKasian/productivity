var gulp = require('gulp');
var sass = require('gulp-sass');
var server = require('gulp-server-livereload');

gulp.task('webserver', function() {
    gulp.src('./app')
        .pipe(server({
            livereload: true,
            directoryListing: false,
            open: true
        }));
});

gulp.task('sass', function () {
    return gulp.src('./app/scss/main.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./app/css'));
});

gulp.task('default', function() {
    gulp.run('webserver', 'sass');
    gulp.watch('./app/scss/*.scss', ['sass']);
});