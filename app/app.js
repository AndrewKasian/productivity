(function () {
    'use strict';

    angular.module('app', [
        'ui.router',
        'ui.bootstrap',
        'ngResource',
        'ngCookies',
        'dndLists'
    ]).config([
        '$locationProvider',
        '$urlRouterProvider',
        function ($locationProvider, $urlRouterProvider) {
            $urlRouterProvider.otherwise('/login'); //Default state login page;
            // $locationProvider.html5Mode(true);
    }])
    .run([
        '$rootScope',
        '$state',
        'AuthService',
        '$http',
        '$cookies',
        function($rootScope, $state, AuthService, $http, $cookies) {
             $rootScope.$on('$stateChangeStart', function(event, next) {
                if (next.authenticate && !AuthService.isAuthenticated) {
                    event.preventDefault();
                    $state.go('login');
                }
            });
            $rootScope.$on('tokenChange', function () {
                $http.defaults.headers.common.Token = $cookies.get('token');
            });
            $rootScope.$emit('tokenChange');
    }])
}());
