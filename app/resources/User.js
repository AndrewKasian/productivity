(function () {
    'use strict';

    /**
     * IN PROGRESS
     */

    angular.module('app')
        .factory('User', ['$resource', 'ApiEndPoint', function ($resource, ApiEndPoint) {
            return $resource(ApiEndPoint + '/user/:id', {}, {
                update: {
                    method: 'PUT'
                }
            });
        }])

}());