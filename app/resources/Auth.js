(function () {
    'use strict';

    angular.module('app')
        .factory('Auth', ['$resource', 'ApiEndPoint', function ($resource, ApiEndPoint) {
            return $resource(ApiEndPoint + '/:auth', {}, {
                signUp: {
                    method: 'POST',
                    params: {
                        auth: 'registration'
                    }
                },
                signIn: {
                    method: 'POST',
                    params: {
                        auth: 'login'
                    }
                }
            });
        }])
}());