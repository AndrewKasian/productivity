(function () {
    'use strict';

    angular.module('app')
        .factory('Profile', ['$resource', 'ApiEndPoint', function ($resource, ApiEndPoint) {
            return $resource(ApiEndPoint + '/profile', {}, {
                update: {
                    method: 'PUT'
                }
        });
    }])

}());