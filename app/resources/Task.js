(function () {
    'use strict';

    var app = angular.module('app');
    app.factory('Task', ['$resource', 'ApiEndPoint',function ($resource, ApiEndPoint) {
        return $resource(ApiEndPoint + '/task', {}, {
            update: {
                method: 'PUT'
            }
        });
    }])
}());