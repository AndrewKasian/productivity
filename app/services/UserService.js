(function () {
    'use strict';

    /**
     * IN PROGRESS
     */

    angular.module('app')
        .service('UserService', ['User', function (User) {

            this.getUsers =  function () {
                return User.query().$promise;
            };

            this.getUserProfile = function () {
                return User.get().$promise;
            };

            this.getUserById = function (id) {
                return User.get({id: id}).$promise;
            };

            this.updateUser = function (userId, user) {
                return User.update({id: userId}, user).$promise;
            };

            this.deleteUser = function (id) {
                return User.remove({id: id}).$promise;
            };
        }])
}());