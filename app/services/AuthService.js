(function () {
    'use strict';

    angular.module('app')
        .service('AuthService', ['Auth', '$q', '$cookies', '$rootScope', function (Auth, $q, $cookies, $rootScope) {

            this.isAuthenticated = false;

            this.login = function (user) {
                var self = this;
                var deferred = $q.defer();
                Auth.signIn(user).$promise.then(function (data) {
                    $cookies.put('token', data.toJSON().token);
                    self.isAuthenticated = true;
                    $rootScope.$emit('tokenChange');
                    deferred.resolve(data.toJSON());
                }, function (err) {
                    deferred.reject(err.data.details);
                });
                return deferred.promise;
            };

            this.signUp = function (user) {
                var self = this;
                var deferred = $q.defer();
                Auth.signUp(user).$promise.then(function (data) {
                    $cookies.put('token', data.toJSON().token);
                    self.isAuthenticated = true;
                    $rootScope.$emit('tokenChange');
                    deferred.resolve(data.toJSON());
                }, function (err) {
                    deferred.reject(err.data.details);
                });
                return deferred.promise;
            };

            this.signOut = function () {
                var deferred = $q.defer();
                $cookies.remove('token');
                this.isAuthenticated = false;
                $rootScope.$emit('tokenChange');
                deferred.resolve();
                return deferred.promise;
            };
        }])
}());