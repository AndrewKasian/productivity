(function () {
    'use strict';

    /**
     * GET /task  => get all tasks
     * POST /task => create new task
     * PUT /task/:id => update task byId
     * GET /task/:id  => get task byId
     * DELETE /task/:id  => delete task byId
     * GET /user/:id/task => tasks associated with concrete user.
     */

    angular.module('app')
        .service('TaskService', ['Task', function (Task) {

            this.getTasks =  function () {
                return Task.query().$promise;
            };

            this.getTasksById = function (taskId) {
                return Task.get({id: taskId}).$promise;
            };

            this.createTask = function (task) {
                return Task.save(task).$promise;
            };

            this.updateTask = function (task) {
                return Task.update(task).$promise;
            };

            this.deleteTask = function (taskId) {
                return Task.remove({id: taskId});
            };

            this.userTasks = function (taskId) {
                return Task.getUserTasks({userId: taskId}).$promise;
            };
        }])
}());