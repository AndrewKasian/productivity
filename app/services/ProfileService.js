(function () {
    'use strict';

    /**
     * GET  => get User profile
     * Put  => update User profile
     */

    angular.module('app')
        .service('ProfileService', ['Profile', function (Profile) {

            this.getUserProfile = function () {
                return Profile.get().$promise;
            };

            this.updateUser = function (user) {
                return Profile.update(user).$promise;
            };

        }])
}());