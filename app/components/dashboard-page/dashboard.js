(function () {
    'use strict';
    angular.module('app')
        .config(['$stateProvider', function ($stateProvider) {
            $stateProvider
                .state('dashboard',{
                    url: '/dashboard',
                    authenticate: true,
                    views: {
                        'header' : {
                            template: '<my-header></my-header>'
                        },
                        'content' : {
                            templateUrl: "components/dashboard-page/dashboard.html",
                            controller: 'UserCtrl',
                            controllerAs: 'dashboard',
                            resolve: {
                                user: ['ProfileService', function (ProfileService) {
                                    return ProfileService.getUserProfile();
                                }]
                            }
                        },
                        'footer' : {
                            template: '<my-footer></my-footer>'
                        }
                    }
                })
        }])
        .controller('UserCtrl',['user', function(user) {
            console.log(user);
            this.user = {
                name: user.name,
                tasksAmount: user.tasksAmount,
                viewTasks: user.viewTasks
            };
        }]);
}());