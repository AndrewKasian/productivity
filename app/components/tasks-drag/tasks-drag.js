(function () {
    'use strict';

    var app = angular.module('app');

    app.config(['$stateProvider', function ($stateProvider) {
        $stateProvider
            .state('drag',{
                url: '/dashboard/tasks-drag',
                authenticate: true,
                views: {
                    'header' : {
                        template: '<my-header></my-header>'
                    },
                    'content' : {
                        templateUrl: "components/tasks-drag/tasks-drag.html",
                        controller: 'TasksCtrl',
                        controllerAs: 'task'
                    },
                    'footer' : {
                        template: '<my-footer></my-footer>'
                    }
                }
            })
    }]);

}());