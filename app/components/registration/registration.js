(function () {
    'use strict';

    var app = angular.module('app');

    app.config(['$stateProvider', function ($stateProvider) {
        $stateProvider
            .state('registration',{
                url: '/registration',
                views: {
                    'header' : {
                        template: '<my-header></my-header>'
                    },
                    'content' : {
                        templateUrl: "components/registration/registration.html",
                        controller: 'RegCtrl',
                        controllerAs: 'reg'
                    },
                    'footer' : {
                        template: '<my-footer></my-footer>'
                    }
                }
            })
    }]);

    app.controller('RegCtrl', ['$state', 'AuthService', function ($state, AuthService) {
        this.newUser = {
            name: null,
            password: null,
            email: null,
            passwordConfirm: null
        };
        this.isPasswordsNotIdentity = false;
        this.checkPasswordsIdentity = function () {
            this.isPasswordsNotIdentity = this.newUser.password !== this.newUser.passwordConfirm;
            this.formRegistration.$valid = !this.isPasswordsNotIdentity;
        };
        this.setFalseIdentity = function () {
            this.isPasswordsIdentity = false;
        };
        this.formSubmit = function () {
            this.checkPasswordsIdentity();
            if(this.formRegistration.$valid){
                console.info('Reg Form is valid');
                AuthService.signUp(this.newUser).then(function () {
                        console.info('Sign up - done!');
                        $state.go('dashboard');
                    }, function (err) {
                        console.log(err);
                    });
            }
        }
    }]);
}());