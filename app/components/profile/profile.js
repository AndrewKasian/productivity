(function () {
    'use strict';

    var app = angular.module('app');

    app.config(['$stateProvider', function ($stateProvider) {
        $stateProvider
            .state('profile',{
                url: '/dashboard/profile',
                authenticate: true,
                views: {
                    'header' : {
                        template: '<my-header></my-header>'
                    },
                    'content' : {
                        templateUrl: "components/profile/profile.html",
                        controller: 'ProfileCtrl',
                        controllerAs: 'profile',
                        resolve: {
                            user: ['ProfileService', function (ProfileService) {
                                return ProfileService.getUserProfile();
                            }]
                        }
                    },
                    'footer' : {
                        template: '<my-footer></my-footer>'
                    }
                }
            })
    }]);

    app.controller('ProfileCtrl', ['user', function(user) {
        this.user = user;
        this.user.viewTasks = user.viewTasks;
        // var self = this;
        // this.taskList = [];
        // TaskService.getTasks().then(function (tasks) {
        //     self.taskList = tasks;
        // }, function (err) {
        //     console.log(err);
        // });
        //
        //
        this.formSubmit = function () {
            if(this.formProfile.$valid){
                console.info('Profile form is valid');
                console.log(this.user);
            }
        };
        //
        // this.changeStatus = function (task) {
        //     TaskService.updateTask(task).then(function (data) {
        //         console.info('Task updated');
        //     })
        // }
    }])

}());