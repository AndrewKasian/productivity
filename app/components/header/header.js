(function () {
    'use strict';

    var app = angular.module('app');

    app.directive('myHeader', ['AuthService', 'ProfileService', '$state', function (AuthService, ProfileService, $state) {
        return {
            restrict: 'E',
            templateUrl: 'components/header/header.html',
            link: function (scope, element, attrs) {
                if(AuthService.isAuthenticated === true){
                    ProfileService.getUserProfile().then(function (user) {
                        scope.user = {
                            name: user.name
                        };
                        scope.isUser = AuthService.isAuthenticated;
                    }, function (err) {
                        console.log(err);
                    });
                }

                scope.signOut = function () {
                    AuthService.signOut().then(function () {
                        $state.go('login');
                    });
                }
            }
        }
    }]);
}());