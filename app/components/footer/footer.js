(function () {
    'use strict';

    var app = angular.module('app');

    app.directive('myFooter', function () {
        return {
            restrict: 'E',
            templateUrl: 'components/footer/footer.html'
        }
    })

}());