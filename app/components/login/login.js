(function () {
    'use strict';

    var app = angular.module('app');

    app.config(['$stateProvider', function ($stateProvider) {
        $stateProvider
            .state('login',{
                url: '/login',
                views: {
                    'header' : {
                        template: '<my-header></my-header>'
                    },
                    'content' : {
                        templateUrl: "components/login/login.html",
                        controller: 'LoginCtrl',
                        controllerAs: 'login'
                    },
                    'footer' : {
                        template: '<my-footer></my-footer>'
                    }
                }
            })
    }]);

    app.controller('LoginCtrl', ['AuthService', '$state', function(AuthService, $state) {
        this.formLogin = null;
        this.user = {
            email: null,
            password: null
        };
        this.formSubmit = function () {
            if(this.formLogin.$valid){
                console.info('Login form is valid');
                AuthService.login(this.user).then(function (data) {
                    console.info('Sign In - done!');
                    //TODO delete after all fix
                    console.log(data);
                    $state.go('dashboard');
                }, function (err) {
                    console.log(err);
                });
            }
        }
    }]);
}());