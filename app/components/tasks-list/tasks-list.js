(function () {
    'use strict';

    var app = angular.module('app');

    app.config(['$stateProvider', function ($stateProvider) {
        $stateProvider
            .state('list',{
                url: '/dashboard/tasks-list',
                authenticate: true,
                views: {
                    'header' : {
                        template: '<my-header></my-header>'
                    },
                    'content' : {
                        templateUrl: "components/tasks-list/tasks-list.html",
                        controller: 'TasksCtrl',
                        controllerAs: 'task'
                    },
                    'footer' : {
                        template: '<my-footer></my-footer>'
                    }
                }
            })
    }]);

    app.controller('TasksCtrl', ['TaskService', '$state', '$scope', 'filterFilter', function(TaskService, $state, $scope, filterFilter) {
        var self = this;
        this.taskList = [];
        TaskService.getTasks().then(function (tasks) {
            self.taskList = tasks;
            if ($state.current.name === 'drag') {
                self.generateDragAndDropList(self.taskList);
            }
        }, function (err) {
            console.log(err);
        });


        this.formSubmit = function () {
            if(this.addTask.$valid){
                console.info('Task form is valid');
                TaskService.createTask({title: this.taskName}).then(function (task) {
                    self.taskList.push(task);
                    self.taskName = '';
                    self.addTask.taskName.$touched = false;
                    
                    if ($state.current.name === 'drag') {
                        $scope.models.lists.Pending.push(task);
                    }
                    
                });
            }
        };

        this.changeStatus = function (task) {
            TaskService.updateTask(task).then(function (item) {
                console.info('Task updated');
                if ($state.current.name === 'drag') {
                    self.updateTaskLists(item);
                }
            })
        };

        this.generateDragAndDropList = function (tasks) {
            $scope.models = {
                selected: null,
                lists: {
                    "Pending": filterFilter(tasks, 'pending'), 
                    "Done": filterFilter(tasks, 'done')
                }
            };
        };
 
        
        this.insertItemCallback = function(item, listName) {
            var listStatus = listName.toLowerCase();
            if (item.status !== listStatus) {
                item.status = listStatus;
                self.changeStatus(item);
            }
        };

        this.updateTaskLists = function (item) {
            for (var i = 0; i < this.taskList.length; i++) {
                if (this.taskList[i]._id === item._id) {
                    this.taskList[i] = item;
                }
            }
        }

    }])

}());